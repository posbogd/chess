﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectPromoFigure : MonoBehaviour
{
    [SerializeField] PromotionPanel panel;
    [SerializeField] Figures figure;
    // Start is called before the first frame update
    private void OnMouseDown()
    {
        panel.SetFigure(figure);
    }

}
