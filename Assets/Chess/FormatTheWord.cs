﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Text;
using System.Linq;
public class FormatTheWord : MonoBehaviour
{
    [SerializeField]
    TMP_InputField inputField;
    [SerializeField]
    TextMeshProUGUI text;
    [SerializeField] Text textUnity;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ChangeToBold()
    {
        var s = inputField.selectionStringAnchorPosition;
        var e = inputField.selectionStringFocusPosition;
        string str = inputField.text;
        string s2, s3;
        if (e > s)
        {
            //str = str.Remove(s, e - s);
            s3 = FormatTag(str, "<b>", "</b>", s, e);
        }
        else
        {
            //str = str.Remove(e, s - e);
            s3 = FormatTag(str, "<b>", "</b>", e, s); //str.Insert(e, s3);
        }
        inputField.text = s3;
        Debug.Log($"{s} {e}");
    }
    public string FormatTag(string s, string tagS, string tagE,int sSubString, int eSubString)
    {
        
        var sPos = GetTagsPositions(s, tagS);
        var ePos = GetTagsPositions(s, tagE);
        List<int> tempS = new List<int>();
        List<int> tempE = new List<int>();
        if(sPos.Count>0&&ePos.Count>0)
        for (int i = 0; i < sPos.Count; i++)
        {
            if ((int)sPos[i] > sSubString&&(int)sPos[i]<eSubString)
                tempS.Add(i);
            if ((int)ePos[i] > sSubString && (int)ePos[i] < eSubString);
                tempE.Add(i);
        }
        var t = tempS.Intersect(tempE).ToList();
        var t2 = tempS.Except(tempE).ToList();
        if (t.Count() > 0 && t2.Count() > 0)
        {
            var wholeList = t.Union(t2).OrderByDescending(x => x).ToList();

        } else
            if (t.Count() == 0 && t2.Count == 0)
        {
            if (tempS.Count() > 0)
            {
                int mS = tempS.Where(x => x < sSubString).Max();
                int mE = tempE.Where(x => x < sSubString).Max();
                if (mS > mE)
                {
                    Debug.Log("S>E");
                    s.Insert(eSubString, tagS);
                    s.Insert(sSubString, tagE);
                }
                else
                {
                    Debug.Log("S<E");
                    s = s.Insert(eSubString, tagE);
                    s = s.Insert(sSubString, tagS);
                }
            }
            else {
                Debug.Log("No tags");
                s = s.Insert(eSubString, tagE);
                s = s.Insert(sSubString, tagS);
            }
        }

        //else
        return s;
    }
    public string FixTags(string s, string sTag, string eTag)
    {
        var sPos = GetTagsPositions(s, sTag);
        var ePos = GetTagsPositions(s, eTag);
        List<int?> uncorrectEndings;
        if (sPos.Count == ePos.Count)
        {
            return s;
        }
        else if (sPos.Count < ePos.Count)
            for (int i = 0; i < sPos.Count; i++)
            {
                if (sPos[i] < ePos[i]) continue;
                else
                {
                    s = s.Remove((int)ePos[i], 4);
                    return s;
                }
            }
        else if (sPos.Count > ePos.Count)
            for (int i = 0; i < ePos.Count; i++)
            {
                if (ePos[i]>sPos[i]) continue;
                else
                {
                    s = s.Remove((int)sPos[i], 3);
                    return s;
                }
            }
        return s;
    }
    public List<int?> GetTagsPositions(string s, string tag)
    {
        List<int?> retVal = new List<int?>();
        for (int i = 0; i < s.Length; i++)
        {
            int n = 0;
            for (int j = 0; j < tag.Count(); j++)
            {
                if (s[i + j] == tag[j]) n++;
            }
            if(n==tag.Count())retVal.Add(i);
        }
        return retVal;
    }
  
}
