﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class FiguresArray
{
    [SerializeField] public Figure figure;

    public FiguresArray(Figure fig)
    {
        figure = fig;
    }
}
