﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knight : Figure
{
    public Knight(Side side, int position) : base(side, position) { }
    public Knight(Side side, int position, bool placeOnBoard) : base(side, position, placeOnBoard) { }
    public override List<int> Moves(int currentPosition)
    {
        var retVal = GetHorseMoves(currentPosition);
        return retVal;
    }
    public override Figures GetFigure()
    {
        return Figures.Horse;
    }
    public override Figure CopyFigure()
    {
        return new Knight(side, position,false);
    }
}
