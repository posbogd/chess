﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rook : Figure
{
    public Rook(Side side, int position) : base(side, position) { }
    public Rook(Side side, int position, bool placeOnBoard) : base(side, position, placeOnBoard) { }
    public override Figures GetFigure()
    {
        return Figures.Rook;
    }
    public override Figure CopyFigure()
    {
        return new Rook(side, position, false);
    }
    public override List<int> Moves(int currentPosition)
    {
        return GetRookMovesNew(currentPosition);
    }
}
