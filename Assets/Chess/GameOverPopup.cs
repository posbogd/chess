﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameOverPopup : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Text _msg;
    public void SetGameOverPopup(string msg)
    {
        _msg.text = msg;
    }
    public void OnQuitClick()
    {
        Application.Quit();
    }
    public void OnNewGame()
    {
        Board.Instance.NewGame();
    }
}
