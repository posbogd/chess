﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move 
{
    public int? startPosition;
    public Figure startFigure;
    public int? endPosition;
    public Figure endFigure;
    public Move(int? sPos, Figure sFigure, int? ePos, Figure eFigure)
    {
        startPosition = sPos;
        startFigure = sFigure;
        endPosition = ePos;
        endFigure = eFigure;
    }
}
