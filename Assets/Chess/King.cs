﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class King : Figure
{
    public King(Side side, int position) : base(side, position) { }
    public King(Side side, int position, bool placeOnBoard) : base(side, position, placeOnBoard) { }
    public override Figures GetFigure()
    {
        return Figures.King;
    }

    public override List<int> Moves(int currentPosition)
    {
        return GetKingMoves(currentPosition);
    }
    public override Figure CopyFigure()
    {
        return new King(side, position,false);
    }
}
