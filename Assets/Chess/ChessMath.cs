﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ChessMath
{
    public static int _boardSize;
    public static bool InRange(int num)
    {
        return num >= 0 && num <= 63;
    }
    public static bool InRange(int i, int j)
    {
        if (i >= 0 && j >= 0 && i<=7 && j<=7)
            return InRange(GetNumber(i, j));
        else
            return false;
    }
    public static int GetJ(int num)
    {
        return num / _boardSize;
    }
    public static int GetI(int num)
    {
        return num - GetJ(num) * _boardSize;
    }
    public static int GetNumber(int i, int j)
    {
        return j * _boardSize + i;
    }
    public static string GetMove(int i, int j)
    {
        string retVal="";
        switch (i)
        {
            case (0):
                retVal += "a";
                break;
            case (1):
                retVal += "b";
                break;
            case (2):
                retVal += "c";
                break;
            case (3):
                retVal += "d";
                break;
            case (4):
                retVal += "e";
                break;
            case (5):
                retVal += "f";
                break;
            case (6):
                retVal += "g";
                break;
            case (7):
                retVal += "h";
                break;
        }
        retVal += (j + 1).ToString();
        return retVal;
    }
    public static string GetMove(int num)
    {
       return GetMove(ChessMath.GetI(num), ChessMath.GetJ(num));
    }
    public static Side RevertSide(Side side)
    {
        if (side == Side.Black) return Side.White;
        else return Side.Black;
    }
}
