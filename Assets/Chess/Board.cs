﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Board : MonoBehaviour
{
    [SerializeField]
    GameObject _tile;
    [SerializeField]
    int _boardSize;
    [SerializeField]
    float _tileSize;
    [SerializeField]
    Transform _parent;
    [SerializeField]
    Transform _gameOverPopup;
    [SerializeField]
    private Transform _canvas;
    public Tile[] board;// = new Tile[64];
    public static Board Instance;
    public Side sideToMove = Side.White;
    public Stage currentStage = Stage.SelectFigure;
    public List<int> AvailableMoves;
    public int selectedFigure = 99;
    public bool isDoubleCkeck = false;
    public List<Move> Moves;
    public int promotionPosition;
    [SerializeField]
    public List<Figure> figures;
    public GameObject PromotionalPanel;
    public float StartPoint { get { return -(_boardSize * _tileSize / 2 - _tileSize / 2); } }
    private void Awake()
    {
        var pp = Instantiate(PromotionalPanel);
        pp.SetActive(false);
        PromotionalPanel = pp;
        figures = new List<Figure>();
        if (Instance == null) Instance = this;
        else Destroy(gameObject);
        ChessMath._boardSize = _boardSize;
        InitBoard();
        SetFiguresToStartPositions();
        //SetMoveData(30, 46);
    }
    public void GameOver(string msg)
    {
        var g = Instantiate(_gameOverPopup, _canvas).GetComponent<GameOverPopup>();
        g.gameObject.SetActive(true);
        g.SetGameOverPopup(msg);
        Debug.Log("Game over function called");
    }
    public void NewGame()
    {
        SceneManager.LoadScene(0);
    }
    public void DisplayFiguresOnBoard()
    {
        foreach (var f in figures)
        {
            Debug.Log($"{f.side.ToString()} {f.GetFigure().ToString()}");
        }
    }

    public void SetFiguresToStartPositions()
    {
         figures = new List<Figure>
        {
            new King(Side.White, ChessMath.GetNumber(4, 0)),
            new King(Side.Black, ChessMath.GetNumber(4, 7)),

            new Rook(Side.White, ChessMath.GetNumber(0, 0)),
            new Rook(Side.White, ChessMath.GetNumber(7, 0)),
            new Knight(Side.White, ChessMath.GetNumber(1, 0)),
            new Knight(Side.White, ChessMath.GetNumber(6, 0)),
            new Bishop(Side.White, ChessMath.GetNumber(2, 0)),
            new Bishop(Side.White, ChessMath.GetNumber(5, 0)),
            new Quin(Side.White, ChessMath.GetNumber(3, 0)),
            new Knight(Side.White, ChessMath.GetNumber(6, 0)),
            
            //pones white
            new Pone(Side.White,ChessMath.GetNumber(0,1)),
            new Pone(Side.White,ChessMath.GetNumber(1,1)),
            new Pone(Side.White,ChessMath.GetNumber(2,1)),
            new Pone(Side.White,ChessMath.GetNumber(3,1)),
            new Pone(Side.White,ChessMath.GetNumber(4,1)),
            new Pone(Side.White,ChessMath.GetNumber(5,1)),
            new Pone(Side.White,ChessMath.GetNumber(6,1)),
            new Pone(Side.White,ChessMath.GetNumber(7,1)),

            new Rook(Side.Black, ChessMath.GetNumber(0, 7)),
            new Rook(Side.Black, ChessMath.GetNumber(7, 7)),
            new Knight(Side.Black, ChessMath.GetNumber(1, 7)),
            new Knight(Side.Black, ChessMath.GetNumber(6, 7)),
            new Bishop(Side.Black, ChessMath.GetNumber(2, 7)),
            new Bishop(Side.Black, ChessMath.GetNumber(5, 7)),
            new Quin(Side.Black, ChessMath.GetNumber(3, 7)),
            new Knight(Side.Black, ChessMath.GetNumber(6, 7)),
            //pones black
            new Pone(Side.Black,ChessMath.GetNumber(0,6)),
            new Pone(Side.Black,ChessMath.GetNumber(1,6)),
            new Pone(Side.Black,ChessMath.GetNumber(2,6)),
            new Pone(Side.Black,ChessMath.GetNumber(3,6)),
            new Pone(Side.Black,ChessMath.GetNumber(4,6)),
            new Pone(Side.Black,ChessMath.GetNumber(5,6)),
            new Pone(Side.Black,ChessMath.GetNumber(6,6)),
            new Pone(Side.Black,ChessMath.GetNumber(7,6)),
        };
        
    }

    public void InitBoard()
    {
        Moves = new List<Move>();
        board = new Tile[_boardSize * _boardSize];
        for (int j = 0; j < _boardSize; j++)
        for (int i = 0; i < _boardSize; i++)
        {
                   
            int tileNumber = j * _boardSize + i;
            var tile = Instantiate(_tile, _parent);
            tile.name = tileNumber.ToString();
            tile.transform.position = new Vector3(StartPoint + i * _tileSize, 0, StartPoint + j * _tileSize);
            board[tileNumber] = tile.GetComponent<Tile>();
            board[tileNumber].Number = tileNumber;
            if (j % 2 == 0)
            {
                board[tileNumber].SetWhite(i % 2 == 0);
            }
            else
            {
                board[tileNumber].SetWhite(i % 2 != 0);
            }
        }  
    }

    public void SelectAvailableMoves(int tileNumber)
    {
       
    }

    public bool InRange(int num)
    {
        return num >= 0 && num <= 63;
    }
    public bool InRange(int i, int j)
    {
        return InRange(GetNumber(i, j));
    }
    public int GetJ(int num)
    {
        return num / _boardSize; 
    }
    public int GetI(int num)
    {
        return num - GetJ(num)*_boardSize;   
    }
    public int GetNumber(int i, int j)
    {
        return j * _boardSize + i;  
    }

    [Photon.Pun.PunRPC]
    void MakeAMove(byte sMove, byte eMove)
    {
        board[eMove].Figure = board[sMove].Figure;
        board[eMove].Figure.position = eMove;
        board[sMove].Figure = null;
        if (sideToMove == Side.Black)
            sideToMove = Side.White;
        else
            sideToMove = Side.Black;
    }
    public void MoveRpc(int sPosition,int ePosition)
    {
        PhotonView pView =PhotonView.Get(this);
        pView.RPC("MakeAMove", RpcTarget.Others, (byte)sPosition, (byte)ePosition);
    }
    public int SetMoveData(int sPosition, int endPosition)
    {
        int retval =(int)(sPosition +(endPosition << 8));
        byte s = (byte)retval;
        byte e = (byte)(retval >> 8);
        return retval; 
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            foreach (var m in Moves)
            {
                string startFigure="";
                string endFigure = "";
                if (m.startFigure != null) startFigure = m.startFigure.GetFigure().ToString();
                if (m.endFigure != null) endFigure = m.endFigure.GetFigure().ToString();
                Debug.Log($"{startFigure[0] }{ChessMath.GetMove((int)m.startPosition)} : {endFigure[0] }{ChessMath.GetMove((int)m.endPosition)}\n");
            }
           
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            foreach (var f in Board.Instance.figures)
            {
                Debug.Log(GetChar(ChessMath.GetI(f.position))+ (ChessMath.GetJ(f.position)+1).ToString() + f.GetFigure().ToString());
            }
        }
    }
    private string GetChar(int position)
    {
        switch (position)
        {
            case 0: return "a";
            case 1: return "b";
            case 2: return "c";
            case 3: return "d";
            case 4: return "e";
            case 5: return "f";
            case 6: return "g";
            case 7: return "h";
            default:return "error";
        }
    }
}
