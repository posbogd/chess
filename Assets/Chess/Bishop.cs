﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bishop : Figure
{
    public Bishop(Side side, int position) : base(side, position) { }
    public Bishop(Side side, int position, bool placeOnBoard) : base(side, position, placeOnBoard) { }
    public override Figures GetFigure()
    {
        return Figures.Bishop;
    }

    public override List<int> Moves(int currentPosition)
    {
        var retVal = GetBishopMovesNew(currentPosition);
        return retVal;
    }
    public override Figure CopyFigure()
    {
        return new Bishop(side, position,false);
    }
}
