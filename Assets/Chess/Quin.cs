﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quin : Figure
{
    public Quin(Side side, int position) : base(side, position) { }
    public Quin(Side side, int position, bool placeOnBoard) : base(side, position, placeOnBoard) { }
    public override Figures GetFigure()
    {
        return Figures.Quin;
    }
    public override Figure CopyFigure()
    {
        return new Quin(side, position, false);
    }
    public override List<int> Moves(int currentPosition)
    {
        var retVal = GetBishopMovesNew(currentPosition);
        retVal.AddRange(GetRookMovesNew(currentPosition));
        return retVal;
    }
}
