﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PromotionPanel : MonoBehaviour
{
    public GameObject[] promoteFigure;
    public SpriteRenderer[] figuresSprites;
    [SerializeField]
    Sprites figuresSpritesLib;
    public void Start()
    {   
    }
    public void OnEnable()
    {
        if (Board.Instance.sideToMove == Side.Black)
        {
            figuresSprites[0].sprite = figuresSpritesLib.GetSprite(Figures.Rook, Side.White);
            figuresSprites[1].sprite = figuresSpritesLib.GetSprite(Figures.Horse, Side.White);
            figuresSprites[2].sprite = figuresSpritesLib.GetSprite(Figures.Bishop, Side.White);
            figuresSprites[3].sprite = figuresSpritesLib.GetSprite(Figures.Quin, Side.White);
        }
        else
        {
            figuresSprites[0].sprite = figuresSpritesLib.GetSprite(Figures.Rook, Side.Black);
            figuresSprites[1].sprite = figuresSpritesLib.GetSprite(Figures.Horse, Side.Black);
            figuresSprites[2].sprite = figuresSpritesLib.GetSprite(Figures.Bishop, Side.Black);
            figuresSprites[3].sprite = figuresSpritesLib.GetSprite(Figures.Quin, Side.Black);
        }
    }
    public void QuinClick()
    {
        SetFigure(Figures.Quin);
    }
    public void BishopClick()
    {
        SetFigure(Figures.Bishop);
    }
    public void KnightClick()
    {
        SetFigure(Figures.Horse);
    }
    public void RookClick()
    {
        SetFigure(Figures.Rook);
    }
    public void SetFigure(Figures type)
    {
        Figure newFigure;
        switch (type)
        {
            case Figures.Rook:
                newFigure = new Rook(ChessMath.RevertSide(Board.Instance.sideToMove), Board.Instance.promotionPosition);
                break;
            case Figures.Horse:
                newFigure = new Knight(ChessMath.RevertSide(Board.Instance.sideToMove), Board.Instance.promotionPosition);
                break;
            case Figures.Bishop:
                newFigure = new Bishop(ChessMath.RevertSide(Board.Instance.sideToMove), Board.Instance.promotionPosition);
                break;
            case Figures.Quin:
                newFigure = new Quin(ChessMath.RevertSide(Board.Instance.sideToMove), Board.Instance.promotionPosition);
                break;
            default: newFigure = new Pone(ChessMath.RevertSide(Board.Instance.sideToMove), Board.Instance.promotionPosition);
                break;
        }
        Board.Instance.figures.Remove(Board.Instance.board[Board.Instance.promotionPosition].Figure);
        Board.Instance.board[Board.Instance.promotionPosition].Figure = newFigure;
        Board.Instance.figures.Add(newFigure);
        gameObject.SetActive(false);
    }
}
