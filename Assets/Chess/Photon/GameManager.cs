﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
namespace Chess
{
    public class GameManager : MonoBehaviourPunCallbacks
    {
        public static GameManager Instance;
        public Transform content;
        public List<GameObject> children = new List<GameObject>();
        [SerializeField]
        private GameObject _roomPrefab;
        private string _selectedRoomName;
        string gameVersion = "1";
        bool isConnecting;
        #region Photon Callbacks
        private void Start()
        {
            PhotonNetwork.NickName = Random.Range(0,1000).ToString();
            Instance = this;
            //DontDestroyOnLoad(this);
        }
        public override void OnJoinedLobby()
        {
            
        }
        public override void OnJoinedRoom()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                Debug.Log("Created lobby");
            }
            else
                Debug.Log("Connected to lobby");
        }
        public override void OnLeftRoom()
        {
            SceneManager.LoadScene(0);      
        }
        public override void OnLeftLobby()
        {
            CreateRoom();
        }
        public override void OnConnectedToMaster()
        {

        }
        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.Log("PUN Basics Tutorial/Launcher:OnJoinRandomFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom");

            // #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
           // PhotonNetwork.CreateRoom(_selectedRoomName, new RoomOptions { MaxPlayers = 2 });
        }
        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            Debug.LogFormat("OnPlayerEnteredRoom(){0}", newPlayer.NickName);
            if (PhotonNetwork.IsMasterClient)
            {
                Debug.LogFormat("OnPlayerEnteredRoom IsMasterClient {0}", newPlayer.NickName);
                LoadArena();
            }
        }
        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            //base.OnCreateRoomFailed(returnCode, message);
        }
        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            Debug.LogFormat("OnPlayerLeftRoom(){0}", otherPlayer.NickName);
            if (PhotonNetwork.IsMasterClient)
            {
                Debug.LogFormat("OnPlayerLeftRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient);
                LoadArena();
            }
        }
        public override void OnRoomListUpdate(List<RoomInfo> roomList)
        {            
           //base.OnRoomListUpdate(roomList);
            foreach (var go in children)
            {
               Destroy(go);
            }
            children = new List<GameObject>();
            foreach (var r in roomList)
            {
               if (!r.IsOpen || !r.IsVisible) continue;
               Room go = Instantiate(_roomPrefab, content).GetComponent<Room>();
               go.SetRoom(r.Name);
               Debug.LogError(r.Name);
               Debug.LogError(r.PlayerCount);
            }
        }
        public void JoinLobbyOnClick()
        {
            if (!PhotonNetwork.InLobby)
            {
                PhotonNetwork.JoinLobby();
            }
        }
        #endregion
        #region Public Methods
        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }
        public void ConnectToRoom(string roomName)
        {
            PhotonNetwork.JoinRoom(roomName);
        }
        public void CreateRoom()
        {
            PhotonNetwork.CreateRoom(_selectedRoomName, new RoomOptions {IsVisible=true,IsOpen=true, MaxPlayers = 2});
            //PhotonNetwork.LoadLevel("Room");
        }
        public void SetRoomName(string roomName)
        {
            _selectedRoomName = roomName;
        }
        public void Connect()
        {
            isConnecting = true;
            if (PhotonNetwork.IsConnected)
            {
                CreateRoom();
                //PhotonNetwork.JoinRandomRoom();
            }
            else
            {
                PhotonNetwork.GameVersion = gameVersion;
                PhotonNetwork.ConnectUsingSettings();
                CreateRoom();
            }

        }
        #endregion
        #region Private Methods
        void LoadArena()
        {
            if (!PhotonNetwork.IsMasterClient)
            {
                Debug.LogError("PhotonNetwork : Trying to Load a level but we are not the master Client");
            }
            Debug.LogFormat("PhotonNetwork: Loading Level: {0}", PhotonNetwork.CurrentRoom.PlayerCount);
            PhotonNetwork.LoadLevel("Room for " + PhotonNetwork.CurrentRoom.PlayerCount);
        }

        #endregion
    }
}