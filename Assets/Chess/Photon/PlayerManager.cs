﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
namespace Chess {
    public class PlayerManager : MonoBehaviourPunCallbacks
    {
        [SerializeField]
        GameObject _beams;
        public float Health = 1f;
        bool _isFiring;
        private void Awake()
        {
            if (!_beams)
            {
                Debug.LogError("<Color=red><a>Missing</a></Color> Beams Reference.", this);
            }
            else {
                _beams.SetActive(false);
            }
        }
        private void Update()
        {
            if (Health <= 0f)
            {
                GameManager.Instance.LeaveRoom();
            }

            if (photonView.IsMine)
            {
                ProcessInputs();
            }
            if (_beams != null && _isFiring != _beams.activeSelf)
            {
                _beams.SetActive(_isFiring);
            }
        }
        void ProcessInputs()
        {
            if (Input.GetButtonDown("Fire1"))
            {
                if (!_isFiring)
                {
                    _isFiring = true;
                }
            }
            if (Input.GetButtonUp("Fire1"))
            {
                if (_isFiring)
                {
                    _isFiring = false;
                }
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            if (!photonView.IsMine)
            {
                return;
            }
            if (!other.name.Contains("Beam"))
            {
                return;
            }
            Health -= 0.1f;
        }
        private void OnTriggerStay(Collider other)
        {
            if (!photonView.IsMine)
            {
                return;
            }
            if (!other.name.Contains("Beam"))
            {
                return;
            }
            Health -= 0.1f * Time.deltaTime;
        }
    }
}