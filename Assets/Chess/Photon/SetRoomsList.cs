﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class SetRoomsList : MonoBehaviourPunCallbacks
{
    public Transform content;
    public List<GameObject> children = new List<GameObject>();
    public List<Chess.Room> rooms= new List<Chess.Room>();
    [SerializeField]
    private GameObject _roomPrefab;
    [SerializeField]
    PhotonView photonView;
    private GameObject RoomPrefab
    {
        get { return _roomPrefab; }
    }
    public override void OnLeftRoom()
    {
        Debug.Log("Room left");
    }
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        //base.OnRoomListUpdate(roomList);
        photonView.RPC(nameof(RemoveOldRooms),RpcTarget.All);
        children = new List<GameObject>();
        foreach (var r in roomList)
        {
            if (!r.IsOpen || !r.IsVisible) continue;
            if (r.PlayerCount < 2)
            {
                Chess.Room go = Instantiate(_roomPrefab, content).GetComponent<Chess.Room>();
                go.SetRoom(r.Name);
            }
            Debug.LogError(r.Name);
            Debug.LogError(r.PlayerCount);
        }
    }
    [PunRPC]
    public void RemoveOldRooms()
    {
        //foreach (var go in children)
        //{
        //    Destroy(go);
        //}
        var removeRooms = new List<Chess.Room>();
        foreach (var roomListing in rooms)
        {
            if (!roomListing.Updated)
            {
                removeRooms.Add(roomListing);
            }
            else
            {
                roomListing.Updated = false;
            }
        }

        foreach (var roomListing in removeRooms)
        {
            GameObject roomListingObj = roomListing.gameObject;
            rooms.Remove(roomListing);
            Destroy(roomListingObj);
            Debug.Log(roomListingObj.name + " is Destroyed");
        }
    }
    private void RoomReceived(RoomInfo room)
    {
        int index = rooms.FindIndex(x => x.RoomName == room.Name);

        if (index == -1)
        {
            if (room.IsVisible && room.PlayerCount < room.MaxPlayers)
            {
                GameObject roomListingObj = Instantiate(_roomPrefab);
                roomListingObj.transform.SetParent(transform, false);

                var roomListing = roomListingObj.GetComponent<Chess.Room>();
                rooms.Add(roomListing);

                index = (rooms.Count - 1);

            }
        }

        if (index != -1)
        {
            var roomListing = rooms[index];
            roomListing.SetRoom(room.Name);
            roomListing.Updated = true;
        }
    }
}
