﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess
{
    
    public class PlayerAnimatorManager : MonoBehaviour
    {
        [SerializeField]
        float _directionDumpTime = 0.25f;
        private Animator animator;
        // Start is called before the first frame update
        void Start()
        {
            animator = GetComponent<Animator>();
            if (!animator)
                Debug.LogError("Player animator manager is missing component", this);
        }

        // Update is called once per frame
        void Update()
        {
            if (!animator) return;
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");
            if (v < 0)
            {
               v = 0;
            }
            animator.SetFloat("Speed", v*v+h*h);
            animator.SetFloat("Direction", h,_directionDumpTime, Time.deltaTime);
            AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
            if (stateInfo.IsName("Base Layer.Run"))
            {
                if (Input.GetButtonDown("Jump"))
                {
                    animator.SetTrigger("Jump");
                }
            }
        }
    }
}