﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateRoom : MonoBehaviourPun
{

    [SerializeField]
    private GameObject LobbyMenu;
    [SerializeField]
    private GameObject RoomManu;
    [SerializeField]
    private Text _roomName;

    private Text RoomName
    {
        get { return _roomName; }
    }
    public void OnClick_CreateRoom()
    {
        RoomOptions roomOptions = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = 4 };
        
        if (PhotonNetwork.CreateRoom(Random.Range(1,10000).ToString(), roomOptions, TypedLobby.Default))
        {
            print("Create room successfully");
            LobbyMenu.SetActive(false);
            RoomManu.SetActive(true);
        }
        else
        {
            print("Create room failed");
        }
    }
    public void OnClick_leaveRoom()
    {
        PhotonNetwork.LeaveRoom();
       // LobbyMenu.SetActive(true);
       // RoomManu.SetActive(false);
    }
    private void OnPhotonCreateRoomFailed(object[] codeAndMessage)
    {
        print("Create Room Failed: " + codeAndMessage[1]);

    }
    private void OnCreatedRoom(short returnCode, string message)
    {
        print("Room Created Successfully");
    }
}
