﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyGameNetwork : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject LobbyMenu;
    [SerializeField]
    private GameObject RoomManu;
    // Start is called before the first frame update
    void Start()
    {
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.OfflineMode = false;
            PhotonNetwork.GameVersion = "0.0.0";
            PhotonNetwork.ConnectUsingSettings();
        }
        else
        {
            Debug.Log("We are connected already.");
        }
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.NickName = Random.Range(1,1000).ToString();

        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }

    public override void OnJoinedLobby()
    {
        print("Joined Lobby");
        if (!PhotonNetwork.InRoom)
        {
            //CanvasManager.Instance.LobbyFunction.transform.SetAsLastSibling();
        }
    }
    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        LobbyMenu.SetActive(true);
        RoomManu.SetActive(false);
    }
}
