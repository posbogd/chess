﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Chess
{
    public class Room : MonoBehaviour
    {
        private Button joinButton;
        [SerializeField]
        private Text _roomNameText;
        public string RoomName;
        public bool Updated = false;
        // Start is called before the first frame update
        void Start()
        {
            joinButton = GetComponent<Button>();
            joinButton.onClick.AddListener(()=> {
                    PhotonNetwork.JoinRoom(RoomName);
                Debug.LogError("Joining the room");
            });
        }
        public void SetRoom(string roomName) {
            RoomName = roomName;
            _roomNameText.text = roomName;
        }
    }
}