﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum Side {
    Black,
    White,
}
public abstract class Figure
{
    public Side side;
    public int position;
    public abstract List<int> Moves(int currentPosition);
    protected Figure(Side side, int position):this(side,position,true){  
    }
    protected Figure(Side side, int position, bool PlaceOnBoard)
    { 
       this.position = position;
       this.side = side;
       if(PlaceOnBoard)
       Board.Instance.board[position].Figure = this;
    }
    public bool Moved = false;
    public abstract Figures GetFigure();
    public abstract Figure CopyFigure();
 
    protected List<int> GetBishopMoves(int currentPosition)
    {
        var retVal = new List<int>();
        var i = ChessMath.GetI(currentPosition);
        var j = ChessMath.GetJ(currentPosition);
        int n = 1;
        while (ChessMath.InRange(i + n, j + n))
        {
            if(AddBishopAndRookMoves(ref retVal,i+n, j+n))
                n++;
            else
            {
                break;
            }
        }
        n = 1;
        while (ChessMath.InRange(i - n, j - n))
        {
            if(AddBishopAndRookMoves(ref retVal,i-n, j-n))
                n++;
            else
            {
                break;
            }
        }
        n = 1;
        while (ChessMath.InRange(i + n, j - n))
        {
            if(AddBishopAndRookMoves(ref retVal,i+n, j-n))
                n++;
            else
            {
                break;
            }
        }
        n = 1;
        while (ChessMath.InRange(i - n, j + n))
        {
            if(AddBishopAndRookMoves(ref retVal,i - n, j + n))
                n++;
            else
            {
                break;
            }
        }
        return retVal;
    }
    bool AddBishopAndRookMoves(ref List<int> retVal,int i,int j)
    {
        //Debug.Log($"({i},{j});");
        var number = ChessMath.GetNumber(i, j);
        if (Board.Instance.board[number].Figure != null)
        {
            if (Board.Instance.board[number].Figure.side != side)
                retVal.Add(number);
            return false;
        } else
            retVal.Add(number);
        return true;
    }
    public List<int> GetHorseMoves(int currentPosition)
    {
        var retVal = new List<int>();
        var i = ChessMath.GetI(currentPosition);
        var j = ChessMath.GetJ(currentPosition);
        var tempI = i-1;
        var tempJ = j+2;
        AddHorseMove(ref retVal, tempI, tempJ);
        tempI = i + 1;
        tempJ = j + 2;
        AddHorseMove(ref retVal, tempI, tempJ);
        tempI = i - 1;
        tempJ = j - 2;
        AddHorseMove(ref retVal, tempI, tempJ);
        tempI = i + 1;
        tempJ = j - 2;
        AddHorseMove(ref retVal, tempI, tempJ);
        tempI = i + 2;
        tempJ = j - 1;
        AddHorseMove(ref retVal, tempI, tempJ);
        tempI = i + 2;
        tempJ = j + 1;
        AddHorseMove(ref retVal, tempI, tempJ);
        tempI = i - 2;
        tempJ = j - 1;
        AddHorseMove(ref retVal, tempI, tempJ);
        tempI = i - 2;
        tempJ = j + 1;
        AddHorseMove(ref retVal, tempI, tempJ);
        return retVal;
    }
    void AddHorseMove(ref List<int> RetVal,int i,int j)
    {
        var number = ChessMath.GetNumber(i, j);
        if (ChessMath.InRange(i, j))
        { if (Board.Instance.board[number].Figure != null)
            {
                if (Board.Instance.board[number].Figure.side != side)
                    RetVal.Add(number);
            }
            else
                RetVal.Add(number);
        }
    }

    bool CheckHorse(ref List<int> RetVal, int i, int j)
    {
        var number = ChessMath.GetNumber(i, j);
        if (ChessMath.InRange(i, j))
        {
            if(Board.Instance.board[number].Figure!=null)
            if(Board.Instance.board[number].Figure.side!=side)
                if (Board.Instance.board[number].Figure.GetFigure() == Figures.Horse)
                {
                    RetVal.Add(number);
                    return true;
                }
        }
        return false;
    }

    public List<int> GetRookMoves(int currentPosition)
    {
        var retVal = new List<int>();
        var i = ChessMath.GetI(currentPosition);
        var j = ChessMath.GetJ(currentPosition);
        int n = 1;
        while (ChessMath.InRange(i + n, j ))
        {
            if(AddBishopAndRookMoves(ref retVal,i+n, j))
                n++;
            else
            {
                break;
            }
        }
        n = 1;
        while (ChessMath.InRange(i - n, j ))
        {
            if(AddBishopAndRookMoves(ref retVal,i-n, j))
                n++;
            else
            {
                break;
            }
        }
        n = 1;
        while (ChessMath.InRange(i , j - n))
        {
            if(AddBishopAndRookMoves(ref retVal,i, j-n))
                n++;
            else
            {
                break;
            }
        }
        n = 1;
        while (ChessMath.InRange(i , j + n))
        {
            if(AddBishopAndRookMoves(ref retVal,i , j + n))
                n++;
            else
            {
                break;
            }
        }
        return retVal;
    }
  
    public List<int> GetKingMoves(int currentPosition)
    {
        var i = ChessMath.GetI(currentPosition);
        var j = ChessMath.GetJ(currentPosition);
        var retVal = new List<int>();
        if (ChessMath.InRange(i , j + 1))
            if(IsFieldSafe(ChessMath.GetNumber(i,j+1))) AddBishopAndRookMoves(ref retVal, i, j+1);
        if (ChessMath.InRange(i + 1, j + 1))
            if(IsFieldSafe(ChessMath.GetNumber(i+1,j+1)))AddBishopAndRookMoves(ref retVal,i + 1, j+1);
        if (ChessMath.InRange(i + 1, j))
            if(IsFieldSafe(ChessMath.GetNumber(i+1,j)))AddBishopAndRookMoves(ref retVal,i + 1, j);
        if (ChessMath.InRange(i + 1, j - 1))
            if(IsFieldSafe(ChessMath.GetNumber(i+1,j-1)))AddBishopAndRookMoves(ref retVal,i + 1, j-1);
        if (ChessMath.InRange(i , j - 1))
            if(IsFieldSafe(ChessMath.GetNumber(i,j-1)))AddBishopAndRookMoves(ref retVal,i , j-1);
        if (ChessMath.InRange(i - 1 , j - 1))
            if(IsFieldSafe(ChessMath.GetNumber(i-1,j-1)))AddBishopAndRookMoves(ref retVal,i - 1, j-1);
        if (ChessMath.InRange(i - 1, j))
            if(IsFieldSafe(ChessMath.GetNumber(i-1,j)))AddBishopAndRookMoves(ref retVal,i - 1, j);
        if (ChessMath.InRange(i - 1, j+1))
            if(IsFieldSafe(ChessMath.GetNumber(i-1,j+1)))AddBishopAndRookMoves(ref retVal,i - 1, j+1);
        var king = Board.Instance.board[currentPosition].Figure;
        if (king != null && !king.Moved)
        {
            var check = GetThreadTiles(position);
            bool isChecked = (check.moves != null) && (check.moves.Count > 0);
            if (!isChecked)
            {
                var cast = CastE(currentPosition);
                if (cast.figureHit != null)
                    if (cast.figureHit.side == king.side)
                    {
                        if (cast.figureHit.GetFigure() == Figures.Rook)
                        {
                            if (cast.figureHit.Moved == false)
                            {
                                retVal.Add(ChessMath.GetNumber(i + 2, j));
                               // if (Board.Instance.board[Board.Instance.selectedFigure].Figure != null && Board.Instance.board[Board.Instance.selectedFigure].Figure.GetFigure() == Figures.King)
                                    Board.Instance.board[ChessMath.GetNumber(i + 2, j)].AdditionalAction =
                                    () =>
                                    {
                                        Board.Instance.board[ChessMath.GetNumber(i + 1, j)].Figure = cast.figureHit;
                                        Board.Instance.board[cast.figureHit.position].Figure = null;
                                        cast.figureHit.position = ChessMath.GetNumber(i + 1, j);
                                    };
                             //   else
                                {
                                 //   Board.Instance.board[ChessMath.GetNumber(i + 2, j)].Castle = null;
                                }
                            }
                        }
                    }
                var cast2 = CastW(currentPosition);
                if (cast2.figureHit != null)
                    if (cast2.figureHit.side == king.side)
                    {
                        if (cast2.figureHit.GetFigure() == Figures.Rook)
                        {
                            if (cast2.figureHit.Moved == false)
                            {
                                retVal.Add(ChessMath.GetNumber(i - 2, j));
                               
                              //  if (Board.Instance.board[Board.Instance.selectedFigure].Figure!=null&&Board.Instance.board[Board.Instance.selectedFigure].Figure.GetFigure() == Figures.King)
                                    Board.Instance.board[ChessMath.GetNumber(i - 2, j)].AdditionalAction =
                                        () =>
                                        {
                                            Board.Instance.board[ChessMath.GetNumber(i - 1, j)].Figure = cast2.figureHit;
                                            Board.Instance.board[cast2.figureHit.position].Figure = null;
                                            cast2.figureHit.position = ChessMath.GetNumber(i - 1, j);
                                        };
                             //   else
                                {
                                    //Board.Instance.board[ChessMath.GetNumber(i - 2, j)].Castle = null;
                                }

                            }
                        }
                    }
            }
        }
        return retVal;
    }

    protected List<int> GetBishopMovesNew(int currentPosition)
    { 
        var retVal = new List<int>();
        var c = CastNE(currentPosition);
        retVal.AddRange(c.moves);
        c = CastNW(currentPosition);
        retVal.AddRange(c.moves);
        c = CastSE(currentPosition);
        retVal.AddRange(c.moves);
        c = CastSW(currentPosition);
        retVal.AddRange(c.moves);
        return retVal;
    }

    public List<int> GetRookMovesNew(int currentPosition)
    {
        var retVal = new List<int>();
        var c = CastN(currentPosition);
        retVal.AddRange(c.moves);
        c = CastE(currentPosition);
        retVal.AddRange(c.moves);
        c = CastS(currentPosition);
        retVal.AddRange(c.moves);
        c = CastW(currentPosition);
        retVal.AddRange(c.moves);
        return retVal;
    }
    public bool AddMoveToDirection(ref Cast retVal, int position, ref int n, int? ignorePosition)
    {
        if (Board.Instance.board[position].Figure != null)
        {
            if (IsNumberIgnored(ignorePosition, ref retVal, ref n, position, this.position))
                return false;
            if (Board.Instance.board[position].Figure.side != side)
            {
                retVal.moves.Add(position);
            }
            retVal.figureHit = Board.Instance.board[position].Figure;
            return true;
        }
        retVal.moves.Add(position);
        retVal.n = n;
        n++;
        return false;
    }
    public Cast CastN(int currentPosition, int? ignorePosition = null)
    {
        var retVal = new Cast();
        var i = ChessMath.GetI(currentPosition);
        var j = ChessMath.GetJ(currentPosition);
        int n = 1;
        while (true)
        {
            if (!ChessMath.InRange(i, j + n)) break;
            var position = ChessMath.GetNumber(i, j+n);
            if (AddMoveToDirection(ref retVal, position, ref n, ignorePosition)) break;
        }
        return retVal;
    }
    public Cast CastNE(int currentPosition, int? ignorePosition = null)
    {
        var retVal = new Cast();
        var i = ChessMath.GetI(currentPosition);
        var j = ChessMath.GetJ(currentPosition);
        int n = 1;
        while (true)
        {
            if (!ChessMath.InRange(i+n, j + n)) break;
            var position = ChessMath.GetNumber(i+n, j+n);
            if (AddMoveToDirection(ref retVal, position, ref n, ignorePosition)) break;
        }
        return retVal;
    }
    public Cast CastE(int currentPosition, int? ignorePosition = null)
    {
        var retVal = new Cast();
        var i = ChessMath.GetI(currentPosition);
        var j = ChessMath.GetJ(currentPosition);
        int n = 1;
        while (true)
        {
            if (!ChessMath.InRange(i+n, j )) break;
            var position = ChessMath.GetNumber(i+n, j);
            if (AddMoveToDirection(ref retVal, position, ref n, ignorePosition)) break;
        }
        return retVal;
    }
    public Cast CastSE(int currentPosition, int? ignorePosition = null)
    {
        var retVal = new Cast();
        var i = ChessMath.GetI(currentPosition);
        var j = ChessMath.GetJ(currentPosition);
        int n = 1;
        while (true)
        {
            if (!ChessMath.InRange(i+n, j - n)) break;
            var position = ChessMath.GetNumber(i+n, j-n);
            if (AddMoveToDirection(ref retVal, position, ref n, ignorePosition)) break;
        }
        return retVal;
    }
    public Cast CastS(int currentPosition, int? ignorePosition = null)
    {
        var retVal = new Cast();
        var i = ChessMath.GetI(currentPosition);
        var j = ChessMath.GetJ(currentPosition);
        int n = 1;
        while (true)
        {
            if (!ChessMath.InRange(i, j - n)) break;
            var position = ChessMath.GetNumber(i, j-n);
            if (AddMoveToDirection(ref retVal, position, ref n, ignorePosition)) break;
        }
        return retVal;
    }
    public Cast CastSW(int currentPosition, int? ignorePosition=null)
    {
        var retVal = new Cast();
        var i = ChessMath.GetI(currentPosition);
        var j = ChessMath.GetJ(currentPosition);
        int n = 1;
        while (true)
        {
            if (!ChessMath.InRange(i-n, j - n)) break;
            var position = ChessMath.GetNumber(i-n, j-n);
            if (AddMoveToDirection(ref retVal, position, ref n, ignorePosition)) break;
        }
        return retVal;
    }
    public Cast CastW(int currentPosition, int? ignorePosition=null)
    {
        var retVal = new Cast();
        var i = ChessMath.GetI(currentPosition);
        var j = ChessMath.GetJ(currentPosition);
        int n = 1;
        while (true)
        {
            if (!ChessMath.InRange(i-n, j )) break;
            var position = ChessMath.GetNumber(i-n, j);
            if (AddMoveToDirection(ref retVal, position, ref n, ignorePosition)) break;
        }
        return retVal;
    }
    public Cast CastNW(int currentPosition, int? ignorePosition = null)
    {
        var retVal = new Cast();
        var i = ChessMath.GetI(currentPosition);
        var j = ChessMath.GetJ(currentPosition);
        int n = 1;
        while (true)
        {
            if (!ChessMath.InRange(i-n, j+n )) break;
            var position = ChessMath.GetNumber(i-n, j+n);
            if (AddMoveToDirection(ref retVal, position, ref n, ignorePosition)) break;
        }
        return retVal;
    }
    public bool IsNumberIgnored(int? ignorePosition,ref Cast retVal,ref int n,int number, int position)
    {
        if (ignorePosition != null)
        {
            if (number == ignorePosition)
            {
                retVal.moves.Add(number);
                n++;
                return true;
            }
        }
        return false;
    }
public void LogCast(int count)
    {
        Debug.Log($"Count");
    }

    public List<int> Horse(int currentPosition)
    {
        var retVal = new List<int>();
        var i = ChessMath.GetI(currentPosition);
        var j = ChessMath.GetJ(currentPosition);
        var tempI = i-1;
        var tempJ = j+2;
        if (CheckHorse(ref retVal, tempI, tempJ)) return retVal;
        tempI = i + 1;
        tempJ = j + 2;
        if(CheckHorse(ref retVal, tempI, tempJ))return retVal;
        tempI = i - 1;
        tempJ = j - 2;
        if(CheckHorse(ref retVal, tempI, tempJ))return retVal;
        tempI = i + 1;
        tempJ = j - 2;
        if(CheckHorse(ref retVal, tempI, tempJ))return retVal;
        tempI = i + 2;
        tempJ = j - 1;
        if(CheckHorse(ref retVal, tempI, tempJ))return retVal;
        tempI = i + 2;
        tempJ = j + 1;
        if(CheckHorse(ref retVal, tempI, tempJ))return retVal;
        tempI = i - 2;
        tempJ = j - 1;
        if(CheckHorse(ref retVal, tempI, tempJ))return retVal;
        tempI = i - 2;
        tempJ = j + 1;
        if(CheckHorse(ref retVal, tempI, tempJ))return retVal;
        return retVal;
    }

    public bool IsFieldSafe(int currentPosition)
    {
        if (GetThreadTiles(currentPosition).moves.Count > 0)
            return false;
        else return true;
    }

    public Cast GetThreadTiles(int currentPosition, int? ignorePosition=null)
    {
        Cast retVal = new Cast();
        retVal.moves = new List<int>();
        Board.Instance.isDoubleCkeck = false;
        //Rook Quin
        var c = CastN(currentPosition, ignorePosition);
        if (c.figureHit != null && c.figureHit.side != side)
            if (c.figureHit.GetFigure() == Figures.Rook || c.figureHit.GetFigure() == Figures.Quin||c.figureHit.GetFigure()==Figures.King)
            {
                if (c.figureHit.GetFigure() == Figures.King)
                {
                    if (c.n == 0)
                    {
                        retVal.moves.AddRange(c.moves);
                        retVal.figureHit = c.figureHit;
                    }
                }
                else
                {
                    retVal.moves.AddRange(c.moves);
                    retVal.figureHit = c.figureHit;
                }
            }
        c = CastE(currentPosition, ignorePosition);
        if (c.figureHit != null && c.figureHit.side != side)
            if (c.figureHit.GetFigure() == Figures.Rook || c.figureHit.GetFigure() == Figures.Quin || c.figureHit.GetFigure() == Figures.King)
            {
                if (c.figureHit.GetFigure() == Figures.King)
                {
                    if (c.n == 0)
                    {
                        retVal.moves.AddRange(c.moves);
                        retVal.figureHit = c.figureHit;
                    }
                }
                else
                {
                    retVal.moves.AddRange(c.moves);
                    retVal.figureHit = c.figureHit;
                }
            }

        c = CastW(currentPosition, ignorePosition);

        if (c.figureHit != null && c.figureHit.side != side)
            if (c.figureHit.GetFigure() == Figures.Rook || c.figureHit.GetFigure() == Figures.Quin || c.figureHit.GetFigure() == Figures.King)
            {
                if (c.figureHit.GetFigure() == Figures.King)
                {
                    if (c.n == 0)
                    {
                        retVal.moves.AddRange(c.moves);
                        retVal.figureHit = c.figureHit;
                    }
                }
                else
                {
                    retVal.moves.AddRange(c.moves);
                    retVal.figureHit = c.figureHit;
                }
            }
        c = CastS(currentPosition, ignorePosition);
        if(c.figureHit!=null&&c.figureHit.side!=side)
            if(c.figureHit.GetFigure()==Figures.Rook||c.figureHit.GetFigure()==Figures.Quin || c.figureHit.GetFigure() == Figures.King)
            {
                if (c.figureHit.GetFigure() == Figures.King)
                {
                    if (c.n == 0)
                    {
                        retVal.moves.AddRange(c.moves);
                        retVal.figureHit = c.figureHit;
                    }
                }
                else
                {
                    retVal.moves.AddRange(c.moves);
                    retVal.figureHit = c.figureHit;
                }
            }
        //Bishop Quin
        c = CastNE(currentPosition, ignorePosition);
        if(c.figureHit!=null&&c.figureHit.side!=side)
            if(c.figureHit.GetFigure()==Figures.Rook||c.figureHit.GetFigure()==Figures.Quin||c.figureHit.GetFigure()==Figures.Pone||c.figureHit.GetFigure()==Figures.King)
                if (retVal.moves.Count == 0)
                {
                    if (c.figureHit.GetFigure() == Figures.Pone)
                    {
                        if ((side == Side.White) && c.n == 0)
                        {
                            retVal.moves.AddRange(c.moves);
                            retVal.figureHit = c.figureHit;
                        }
                    }
                    else
                    if (c.figureHit.GetFigure() == Figures.King)
                    {
                        if (c.n == 0)
                        {
                            retVal.moves.AddRange(c.moves);
                            retVal.figureHit = c.figureHit;
                        }
                    }
                    else
                    {
                        retVal.moves.AddRange(c.moves);
                        retVal.figureHit = c.figureHit;
                    }
                }
                else
                    Board.Instance.isDoubleCkeck = true;
        c = CastSE(currentPosition, ignorePosition);
        if(c.figureHit!=null&&c.figureHit.side!=side)
            if(c.figureHit.GetFigure()==Figures.Bishop||c.figureHit.GetFigure()==Figures.Quin || c.figureHit.GetFigure() == Figures.Pone||c.figureHit.GetFigure()==Figures.King)
                if (retVal.moves.Count == 0)
                {
                    if (c.figureHit.GetFigure() == Figures.Pone)
                    {
                        if ((side == Side.Black) && c.n == 0)
                        {
                            retVal.moves.AddRange(c.moves);
                            retVal.figureHit = c.figureHit;
                        }
                    }
                    else
                    if (c.figureHit.GetFigure() == Figures.King)
                    {
                        if (c.n == 0)
                        {
                            retVal.moves.AddRange(c.moves);
                            retVal.figureHit = c.figureHit;
                        }
                    }
                    else
                    {
                        retVal.moves.AddRange(c.moves);
                        retVal.figureHit = c.figureHit;
                    }
                }
                else
                    Board.Instance.isDoubleCkeck = true;
        c = CastSW(currentPosition, ignorePosition);
        if(c.figureHit!=null&&c.figureHit.side!=side)
            if(c.figureHit.GetFigure()==Figures.Bishop||c.figureHit.GetFigure()==Figures.Quin || c.figureHit.GetFigure() == Figures.Pone||c.figureHit.GetFigure()==Figures.King)
                if (retVal.moves.Count == 0)
                {
                    if (c.figureHit.GetFigure() == Figures.Pone)
                    {
                        if ((side == Side.Black) && c.n == 0)
                        {
                            retVal.moves.AddRange(c.moves);
                            retVal.figureHit = c.figureHit;
                        }
                    }
                    else
                    if (c.figureHit.GetFigure() == Figures.King)
                    {
                        if (c.n == 0)
                        {
                            retVal.moves.AddRange(c.moves);
                            retVal.figureHit = c.figureHit;
                        }
                    }
                    else
                    {
                        retVal.moves.AddRange(c.moves);
                        retVal.figureHit = c.figureHit;
                    }
                }
                else
                    Board.Instance.isDoubleCkeck = true;
        c = CastNW(currentPosition, ignorePosition);
        if (c.figureHit != null && c.figureHit.side != side)
            if (c.figureHit.GetFigure() == Figures.Bishop || c.figureHit.GetFigure() == Figures.Quin || c.figureHit.GetFigure() == Figures.Pone||c.figureHit.GetFigure()==Figures.King)
                if (retVal.moves.Count == 0)
                {
                    if (c.figureHit.GetFigure() == Figures.Pone)
                    {
                        if ((side == Side.White) && c.n == 0)
                        {
                            retVal.moves.AddRange(c.moves);
                            retVal.figureHit = c.figureHit;
                        }
                    }
                    else
                    if (c.figureHit.GetFigure() == Figures.King)
                    {
                        if (c.n == 0)
                        {
                            retVal.moves.AddRange(c.moves);
                            retVal.figureHit = c.figureHit;
                        }
                    }
                    else
                    {
                        retVal.moves.AddRange(c.moves);
                        retVal.figureHit = c.figureHit;
                    }
                }
                else
                    Board.Instance.isDoubleCkeck = true;
        var horse = Horse(currentPosition);
        if( horse.Count>0)
        {
            if(retVal.moves.Count==0)
            {
                retVal.moves.AddRange(c.moves);
                retVal.figureHit = c.figureHit;
            }
            else 
                    Board.Instance.isDoubleCkeck = true;
        }
        
        return retVal;
    }
}
