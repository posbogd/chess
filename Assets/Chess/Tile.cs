﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Http.Headers;
using UnityEngine;
using System.Linq;
using Photon.Pun;
using System;

public class Tile : MonoBehaviour
{
    public Action AdditionalAction;
    [SerializeField]
    Sprites figuresSprites;
    [SerializeField]
    SpriteRenderer _figureRenderer;
    [SerializeField] private SpriteRenderer _availableMove;
    private Figure _figure;
    public Figure Figure {
        get { return _figure; }
        set {
            if (value != null)
                _figureRenderer.sprite = figuresSprites.GetSprite(value.GetFigure(), value.side);
            else
                _figureRenderer.sprite = null;
            _figure = value;
        }
    }
    private int startPosition;
    public int Number { get; set;}
    [SerializeField]
    Material _black;
    [SerializeField]
    Material _white;
    public void SetWhite(bool isWhite)
    {
        if (isWhite)
            GetComponent<Renderer>().material = _black;
        else
            GetComponent<Renderer>().material = _white;
    }
    public void Start()
    {
      //  Board.Instance.board[0].Figure.position = 17;
    }
    public void OnMouseDown()
    {
        if (Board.Instance.currentStage == Stage.SelectFigure)
        {
           // AdditionalAction = null;
            SelectFigure();
        }
        else if (Board.Instance.currentStage == Stage.PutFigure)
            {
                if (Board.Instance.AvailableMoves.Contains(Number))
                {
                ClearAllMoves();
                var oldFigure = Board.Instance.board[Number].Figure;
                    if (oldFigure != null) Board.Instance.figures.Remove(oldFigure);
                //Save move ---
                var sFigure = Board.Instance.board[Board.Instance.selectedFigure].Figure != null ? Board.Instance.board[Board.Instance.selectedFigure].Figure.CopyFigure() : null;
                var eFigure = Board.Instance.board[Number].Figure != null ? Board.Instance.board[Number].Figure.CopyFigure() : null;
                Board.Instance.Moves.Add(new Move(Board.Instance.selectedFigure, sFigure, Number, eFigure));
                //-------------
                    Board.Instance.board[Board.Instance.selectedFigure].Figure.position=Number;
                    Board.Instance.board[Board.Instance.selectedFigure].Figure.Moved =true;
                    Board.Instance.board[Number].Figure = Board.Instance.board[Board.Instance.selectedFigure].Figure;
                  //  Board.Instance.board[Number].Figure.position = Number;
                  //  Board.Instance.board[Number].Figure.Moved = true;
                    Board.Instance.board[Board.Instance.selectedFigure].Figure = null;
                    if (Board.Instance.sideToMove == Side.White) Board.Instance.sideToMove = Side.Black;
                    else Board.Instance.sideToMove = Side.White;
                    AdditionalAction?.Invoke();
                    AdditionalAction = null;
                Board.Instance.currentStage = Stage.SelectFigure;
               // Board.Instance.MoveRpc(Board.Instance.selectedFigure, Number);
                } else
                SelectFigure();
                if (IsMate()) Board.Instance.GameOver($"Check mate {Board.Instance.sideToMove.ToString()} lose");
            }
        //Board.Instance.DisplayFiguresOnBoard();
    }
    public bool IsDraw(Side currentSide)
    {
        foreach (var f in Board.Instance.figures)
        {
            if(f.side ==currentSide)
            if (f.Moves(f.position).Count > 0)
            {
                return false;
            }
        }
        Board.Instance.GameOver("Draw");
        return true;
    }

    public void SelectFigure()
    {
        ClearAllMoves();
        Cast check;
        if (Board.Instance.sideToMove == Side.White)
            {
            check = Board.Instance.figures[0].GetThreadTiles(Board.Instance.figures[0].position);
            } else
            check = Board.Instance.figures[1].GetThreadTiles(Board.Instance.figures[1].position);
        if ((check != null) && check.moves.Count > 0)
        {
            if (Board.Instance.isDoubleCkeck)
            {
                if ((Figure.GetFigure() == Figures.King) && (Figure.side == Board.Instance.sideToMove))
                {
                    if (Figure.Moves(Number) != null && Figure.Moves(Number).Count > 0)
                    {
                        SetMoves(Figure.Moves(Number));
                    }
                    else Debug.Log("Check mate");
                }
            }
            else
            {
                List<int> posibleMoves = new List<int>();
                foreach (var f in Board.Instance.figures)
                {
                    if (f.side == Board.Instance.sideToMove)
                    {
                        //Крити шах
                        if (f.GetFigure() != Figures.King)
                            posibleMoves.AddRange(check.moves.Intersect(f.Moves(f.position)));
                        else
                            posibleMoves.AddRange(f.Moves(f.position));
                    }
                }
                if (posibleMoves.Count == 0)
                {
                    Debug.Log("Check mate");
                    return;
                }
                if ((Figure != null) && Figure.side == Board.Instance.sideToMove)
                {
                    if (Figure.GetFigure() != Figures.King)
                    {
                        var c = check.moves.Intersect(Figure.Moves(Number)).ToList();
                        SetMoves(c);
                    }
                    else
                    {
                        /*
                        if (Board.Instance.sideToMove == Side.White)
                        {
                            check = Board.Instance.figures[0].GetThreadTiles(Board.Instance.figures[0].position, Number);
                        }
                        else
                            check = Board.Instance.figures[1].GetThreadTiles(Board.Instance.figures[1].position, Number);
                        if (check.Count == 0)*/
                        SetMoves(Figure.Moves(Number));
                    }
                }
            }
        }
        else
        {
            if (Figure != null)
                if (Board.Instance.sideToMove == Figure.side)
                {
                    // Чи нема прихованого шаху
                    if (Board.Instance.sideToMove == Side.White)
                    {
                        check = Board.Instance.figures[0].GetThreadTiles(Board.Instance.figures[0].position, Number);
                    }
                    else
                        check = Board.Instance.figures[1].GetThreadTiles(Board.Instance.figures[1].position, Number);
                    if (check.moves.Count == 0)
                        SetMoves(Figure.Moves(Number));
                    else
                        SetMoves(check.moves.Intersect(Figure.Moves(Number)).ToList());
                }
        }
    }
    private void SetMoves(List<int> moves)
    {
        Board.Instance.selectedFigure = Number;
        Board.Instance.AvailableMoves = moves;
        SetAvailableMovesSprites(moves);
        Board.Instance.currentStage = Stage.PutFigure;
    }
    public void SetAvailableMove(MoveStatus moveStatus)
    {
        _availableMove.sprite = figuresSprites.GetMoveSprite(moveStatus);
    }
    public void SetAvailableMovesSprites(List<int> moves)
    {
        foreach (var m in moves)
        {
            if(Board.Instance.board[m].Figure==null)
                Board.Instance.board[m].SetAvailableMove(MoveStatus.AvailableTile);
            else
                Board.Instance.board[m].SetAvailableMove(MoveStatus.HitTile);
        }
    }
    public void ClearAllMoves()
    {
        for (int i = 0; i < Board.Instance.board.Length; i++)
        {
            Board.Instance.board[i].SetAvailableMove(MoveStatus.None);
        }
    }
    public bool IsMate()
    {
        Cast check;
        if (Board.Instance.sideToMove == Side.White)
        {
            check = Board.Instance.figures[0].GetThreadTiles(Board.Instance.figures[0].position);
        }
        else
            check = Board.Instance.figures[1].GetThreadTiles(Board.Instance.figures[1].position);
        if ((check != null) && check.moves.Count > 0)
        {
            if (Board.Instance.isDoubleCkeck)
            {
                if ((Figure.GetFigure() == Figures.King) && (Figure.side == Board.Instance.sideToMove))
                {
                    if (Figure.Moves(Number) != null && Figure.Moves(Number).Count > 0)
                    {
                        return false;
                        //SetMoves(Figure.Moves(Number));
                    }
                    else return true;
                }
            }
            else
            {
                List<int> posibleMoves = new List<int>();
                foreach (var f in Board.Instance.figures)
                {
                    if (f.side == Board.Instance.sideToMove)
                    {
                        if (f.GetFigure() != Figures.King)
                        {
                            if (check.moves.Intersect(f.Moves(f.position)).Count() > 0)
                            {
                                Debug.Log(f.position + f.GetFigure().ToString());
                            }
                            posibleMoves.AddRange(check.moves.Intersect(f.Moves(f.position)));
                            
                        }
                        else
                            posibleMoves.AddRange(f.Moves(f.position));
                    }
                }
                if (posibleMoves == null || posibleMoves.Count == 0)
                {
                   // Debug.Log("Check mate");
                    return true;
                }
            }
        }
        else {
            IsDraw(Board.Instance.sideToMove);
            foreach (var t in Board.Instance.board)
            {
                t.AdditionalAction = null;
            }
        }
        return false;
    }
}
