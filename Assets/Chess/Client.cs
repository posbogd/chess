﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Client : MonoBehaviour
{
    private const int MAX_CONNECTIONS = 100;
    private const string SERVER_IP = "127.0.0.1";
    private const int SERVER_PORT = 8999;
    private const int SERVER_WEB_PORT = 8998;
    private const int BUFFER_SIZE = 1024;

    //Channels
    private byte reliableChanelId;
    private byte unreliableChangel;

    //Host
    private int hostId;
    private int connectionId;

    // Logic
    private byte[] buffer = new byte[BUFFER_SIZE];
    private bool isConnected;
    private byte error;
    private void Start()
    {
        GlobalConfig config = new GlobalConfig();
        NetworkTransport.Init(config);

        //host topology
        ConnectionConfig cc = new ConnectionConfig();
        reliableChanelId = cc.AddChannel(QosType.Reliable);
        unreliableChangel = cc.AddChannel(QosType.Unreliable);
        HostTopology topo = new HostTopology(cc, MAX_CONNECTIONS);

        //Connecting to host
        hostId = NetworkTransport.AddHost(topo, 0);

#if UNITY_WEBGL
        connectionId = NetworkTransport.Connect(hostId, SERVER_IP, SERVER_WEB_PORT, 0, out error);
#else
        //Standalone Client
        NetworkTransport.Connect(hostId, SERVER_IP, SERVER_PORT, 0, out error);
#endif
    }
}
