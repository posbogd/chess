﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pone : Figure
{

    public Pone(Side side, int position) : base(side, position) { }
    public Pone(Side side, int position, bool placeOnBoard) : base(side, position, placeOnBoard) { }
    public override Figures GetFigure()
    {
        return Figures.Pone;
    }
    public override Figure CopyFigure()
    {
        return new Pone(side, position,false);
    }
    public override List<int> Moves(int currentPosition)
    {
        var retVal = new List<int>();
        var i = ChessMath.GetI(currentPosition);
        var j = ChessMath.GetJ(currentPosition);
        if (side == Side.White)
        {
            if (ChessMath.InRange(i, j + 1))
                if (Board.Instance.board[ChessMath.GetNumber(i, j + 1)].Figure == null)
                {
                    retVal.Add(ChessMath.GetNumber(i, j + 1));
                    SetPromoPone(Board.Instance.sideToMove, i, j + 1, j);
                    if (j == 1)
                    {
                        if (ChessMath.InRange(i, j + 2))
                            if (Board.Instance.board[ChessMath.GetNumber(i, j + 2)].Figure == null)
                            {
                                retVal.Add(ChessMath.GetNumber(i, j + 2));
                            }
                    }
                }
            if (ChessMath.InRange(i + 1, j + 1))
                if (Board.Instance.board[ChessMath.GetNumber(i + 1, j + 1)].Figure != null && Board.Instance.board[ChessMath.GetNumber(i + 1, j + 1)].Figure.side == Side.Black)
                {
                    retVal.Add(ChessMath.GetNumber(i + 1, j + 1));
                    SetPromoPone(Board.Instance.sideToMove, i + 1, j + 1, j);
                }
            if (ChessMath.InRange(i - 1, j + 1))
                if (Board.Instance.board[ChessMath.GetNumber(i - 1, j + 1)].Figure != null && Board.Instance.board[ChessMath.GetNumber(i - 1, j + 1)].Figure.side == Side.Black)
                {
                    retVal.Add(ChessMath.GetNumber(i - 1, j + 1));
                    SetPromoPone(Board.Instance.sideToMove, i - 1, j + 1, j);
                }
            if (j == 4)
            {
                Move m = Board.Instance.Moves[Board.Instance.Moves.Count - 1];
                if (m.startFigure != null && m.startFigure.GetFigure() == Figures.Pone)
                {
                    if (ChessMath.GetJ((int)m.endPosition) == 4)
                    {
                        if (ChessMath.GetI((int)m.endPosition) == i - 1 || ChessMath.GetI((int)m.endPosition) == i + 1)
                        {
                            int hitPos = ChessMath.GetNumber(ChessMath.GetI((int)m.endPosition), ChessMath.GetJ((int)m.endPosition) + 1);
                            retVal.Add(hitPos);
                            Board.Instance.board[hitPos].AdditionalAction = () =>
                            {
                                Board.Instance.figures.Remove(Board.Instance.board[(int)m.endPosition].Figure);
                                Board.Instance.board[(int)m.endPosition].Figure = null;
                            };
                        }
                    }
                }
            }
        }
        else
        {
            if (ChessMath.InRange(i, j - 1))
                if (Board.Instance.board[ChessMath.GetNumber(i, j - 1)].Figure == null)
                {
                    retVal.Add(ChessMath.GetNumber(i, j - 1));
                    SetPromoPone(Board.Instance.sideToMove, i, j - 1, j);
                    if (j == 6)
                    {
                        if (Board.Instance.board[ChessMath.GetNumber(i, j - 2)].Figure == null)
                        {
                            retVal.Add(ChessMath.GetNumber(i, j - 2));
                        }
                    }
                }
            if (ChessMath.InRange(i - 1, j - 1))
                if (Board.Instance.board[ChessMath.GetNumber(i - 1, j - 1)].Figure != null && Board.Instance.board[ChessMath.GetNumber(i - 1, j - 1)].Figure.side == Side.White)
                {
                    retVal.Add(ChessMath.GetNumber(i - 1, j - 1));
                    SetPromoPone(Board.Instance.sideToMove, i - 1, j - 1, j);
                }
            if (ChessMath.InRange(i + 1, j - 1))
                if (Board.Instance.board[ChessMath.GetNumber(i + 1, j - 1)].Figure != null && Board.Instance.board[ChessMath.GetNumber(i + 1, j - 1)].Figure.side == Side.White)
                {
                    retVal.Add(ChessMath.GetNumber(i + 1, j - 1));
                    SetPromoPone(Board.Instance.sideToMove, i + 1, j - 1, j);
                }
            if (j == 3)
            {
                Move m = Board.Instance.Moves[Board.Instance.Moves.Count - 1];
                if (m.startFigure != null && m.startFigure.GetFigure() == Figures.Pone)
                {
                    if (ChessMath.GetJ((int)m.endPosition) == 3)
                    {
                        if (ChessMath.GetI((int)m.endPosition) == i - 1 || ChessMath.GetI((int)m.endPosition) == i + 1)
                        {
                            int hitPos = ChessMath.GetNumber(ChessMath.GetI((int)m.endPosition), ChessMath.GetJ((int)m.endPosition) - 1);
                            retVal.Add(hitPos);
                            Board.Instance.board[hitPos].AdditionalAction = () =>
                            {
                                Board.Instance.figures.Remove(Board.Instance.board[(int)m.endPosition].Figure);
                                Board.Instance.board[(int)m.endPosition].Figure = null;
                            };
                        }
                    }
                }
            }
        }
        return retVal;
    }
    public void SetPromoPone(Side side, int i, int j, int currentJ)
    {
        if (side == Side.Black)
        {
            if (currentJ == 1)
            {
                Board.Instance.board[ChessMath.GetNumber(i, j)].AdditionalAction = () =>
                {
                    Board.Instance.promotionPosition = ChessMath.GetNumber(i, j);
                    Board.Instance.PromotionalPanel.SetActive(true);
                };
            }
        }
        else
        {
            if (currentJ == 6)
            {
                Board.Instance.board[ChessMath.GetNumber(i, j)].AdditionalAction = () =>
                {
                    Board.Instance.promotionPosition = ChessMath.GetNumber(i, j);
                    Board.Instance.PromotionalPanel.SetActive(true);
                };
            }
        }
    }


}
