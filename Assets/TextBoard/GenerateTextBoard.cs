﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateTextBoard : MonoBehaviour
{
    public float lineWidth;
    public int size;
    public GameObject tilePrefab;
    // Start is called before the first frame update
    public TextTile[,] textBoard;
    void Awake()
    {
        textBoard = new TextTile[size,size];
        for (int i = 0; i < size; i++)
        for (int j = 0; j < size; j++)
        {
            textBoard[i, j] = Instantiate(tilePrefab, transform).GetComponent<TextTile>();
            textBoard[i, j].gameObject.transform.localPosition = new Vector3(i+lineWidth*i,j+lineWidth*j);
            textBoard[i, j].position.text = $"({i};{j})";
        }
    }

    public void SetText( int[,] changedValues)
    {
        for (int i = 0; i < size; i++)
        for (int j = 0; j < size; j++)
        {
            textBoard[i, j].value.text = changedValues[i, j].ToString();
        }
    }
}
