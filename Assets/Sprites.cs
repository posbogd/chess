﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Figures
{
    Pone,
    Rook,
    Horse,
    Bishop,
    Quin,
    King,
}

public enum MoveStatus
{
    None,
    AvailableTile,
    HitTile
}

[CreateAssetMenu(fileName = "Figures", menuName = "Figures", order = 51)]
public class Sprites : ScriptableObject
{
    [SerializeField]
    private  List<Sprite> figures;
    [SerializeField] 
    private List<Sprite> moveSprites;
    public Sprite GetSprite(Figures figure, Side side)
    {
        Sprite s=null;
        switch (figure)
        {
            case Figures.Pone:
                s = (side == Side.White) ? figures[0] : figures[6];
                return s;
            case Figures.Rook:
                s = (side == Side.White) ? figures[1] : figures[7];
                return s;
            case Figures.Horse:
                s = (side == Side.White) ? figures[2] : figures[8];
                return s;
            case Figures.Bishop:
                s = (side == Side.White) ? figures[3] : figures[9];
                return s;
            case Figures.Quin:
                s = (side == Side.White) ? figures[4] : figures[10];
                return s;
            case Figures.King:
                s = (side == Side.White) ? figures[5] : figures[11];
                return s;
            default: return null;
        }
    }
    public Sprite GetMoveSprite(MoveStatus moveStatus)
    {
        switch (moveStatus)
        {
            case MoveStatus.None:
                return null;
            case MoveStatus.AvailableTile: return moveSprites[0];
            case MoveStatus.HitTile: return moveSprites[1];
            default: return null;
        }
    }
}
