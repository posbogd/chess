﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
//using UnityEditor.Experimental.GraphView;

public class Task : MonoBehaviour
{
    public int[,] currentValues = new int[5,5];
    public int[,] changedValues = new int[5,5];
    [SerializeField] private GenerateTextBoard currentBoard;
    [SerializeField] private GenerateTextBoard changedBoard;
    private Dictionary<int, Tuple<int, int>> _dict=new Dictionary<int, Tuple<int, int>>();
    // Start is called before the first frame update
    void Start()
    {
        currentValues[0, 0] = 1;
        currentValues[1, 0] = 0;
        currentValues[2, 0] = 2;
        currentValues[3, 0] = 0;
        currentValues[4, 0] = 0;
        currentValues[0, 1] = 0;
        currentValues[1, 1] = 3;
        currentValues[2, 1] = 0;
        currentValues[3, 1] = 4;
        currentValues[4, 1] = 0;
        currentValues[0, 2] = 0;
        currentValues[1, 2] = 0;
        currentValues[2, 2] = 9;
        currentValues[3, 2] = 0;
        currentValues[4, 2] = 0;
        currentValues[0, 3] = 0;
        currentValues[1, 3] = 0;
        currentValues[2, 3] = 0;
        currentValues[3, 3] = 0;
        currentValues[4, 3] = 0;
        currentValues[0, 4] = 8;
        currentValues[1, 4] = 0;
        currentValues[2, 4] = 0;
        currentValues[3, 4] = 0;
        currentValues[4, 4] = 0;
        for(int y=0;y<currentValues.GetLength(1);y++)
        for(int x=0;x<currentValues.GetLength(0);x++)
        {
            if(currentValues[x,y]!=0)
                _dict[currentValues[x,y]] = new Tuple<int, int>(x,y);
            //  Debug.Log($"Element({currentValues[x,y]})=({x};{y})");
        }
        
        _dict = RemoveNotNeededLines(_dict);
        for(int y=0;y<currentValues.GetLength(1);y++)
        for (int x = 0; x < currentValues.GetLength(0); x++)
        {
            if (_dict.FirstOrDefault(el => el.Value.Item1 == x && el.Value.Item2 == y).Value != null)
                changedValues[x, y] = _dict.FirstOrDefault(el => el.Value.Item1 == x && el.Value.Item2 == y).Key;
            else
            {
                changedValues[x, y] = 0;
            }
        }
        currentBoard.SetText(currentValues);
        changedBoard.SetText(changedValues);
    }

    private Dictionary<int, Tuple<int, int>> RemoveNotNeededLinesNew(Dictionary<int, Tuple<int, int>> sortedInp)
    {
        var max = FindMax(sortedInp);
        int maxX = max.Item1;
        int maxY = max.Item2;
        var sorted2 = new Dictionary<int, Tuple<int, int>>(sortedInp);
        if (maxY >= 2)
        {
            for (int y = 2; y < maxY; y++)
            {
                Debug.Log(y);
                for (int z = y-1; z > 0; z--)
                {
                    Debug.Log(z);
                    bool mergeLane = true;
                    foreach (var element in sortedInp.Where(el => el.Value.Item2 == y))
                    {
                        if (sortedInp.FirstOrDefault(el =>
                                    el.Value.Item1 == element.Value.Item1 &&
                                    el.Value.Item2 == z)
                                .Value != null)
                            mergeLane = false;
                        break;
                    }
                    Debug.Log(mergeLane);
                    if (mergeLane)
                    {
                        foreach (var element in sortedInp.Where(el => el.Value.Item2 == y))
                        {
                            sorted2[element.Key] = new Tuple<int, int>(element.Value.Item1, z);
                        }
                        sortedInp=new Dictionary<int, Tuple<int, int>>(sorted2);
                    }
                }
            }
        }

        return sortedInp;
    }

    private Dictionary<int, Tuple<int, int>> RemoveNotNeededLines(Dictionary<int, Tuple<int, int>> sortedInp)
    {
                var max = FindMax(sortedInp);
                int maxX = max.Item1;
                int maxY = max.Item2;
                //  sortedInp = RemoveEmptyLines(sortedInp);
                var sorted2 = new Dictionary<int, Tuple<int, int>>(sortedInp);
                //  Combine line with bottom one if possible
                if(maxY>=2)
                    for (int y = 2; y < maxY; y++)
                    {
                        bool mergeLine = true;
                        int step = FindStep(sortedInp,y-1);
                        Debug.Log($"Step = {step}; y = {y}");
                        foreach (var element in sortedInp.Where(el => el.Value.Item2 == y))
                        {
                            if (sortedInp.FirstOrDefault(el =>
                                        el.Value.Item1 == element.Value.Item1 &&
                                        el.Value.Item2 == element.Value.Item2 -y+step)
                                    .Value != null)
                                mergeLine = false;
                        }

                        if (mergeLine)
                        {
                            maxY--;
                            foreach (var element in sortedInp.Where(el => el.Value.Item2 >= y))
                            {
                                sorted2[element.Key] =
                                    new Tuple<int, int>(element.Value.Item1, element.Value.Item2 - y+step);
                            }
                            y--;
                            sortedInp = new Dictionary<int, Tuple<int, int>>(sorted2);
                        }
                    }

//return sortedInp;
                return RemoveEmptyLines(sortedInp);
            }

    private int FindStep(Dictionary<int, Tuple<int, int>> sorted, int y)
    {
        if(sorted.Count(el => el.Value.Item2 == y) != 0)
            return y;
        return FindStep(sorted, y - 1);
    }

    private Tuple<int,int> FindMax(Dictionary<int, Tuple<int, int>> sorted)
    {
        int maxX=0;
        int maxY=0;
        foreach (var keyValuePair in sorted)
        {
            maxX= maxX > keyValuePair.Value.Item1?maxX : keyValuePair.Value.Item1;
            maxY= maxY > keyValuePair.Value.Item2?maxY : keyValuePair.Value.Item2;
        }
        Debug.Log(maxY);
        return new Tuple<int, int>(maxX+1,maxY+1);
    }

    private Dictionary<int, Tuple<int, int>> RemoveEmptyLines(Dictionary<int, Tuple<int, int>> sortedInp)
    {
        var t = FindMax(sortedInp);
        int  maxX=t.Item1;
        int maxY=t.Item2;
        var sorted2 = new Dictionary<int, Tuple<int, int>>(sortedInp);

        if(maxY>=1)
            for (int y = 1; y < maxY; y++)
            {
                bool removeLine = sortedInp.Count(el => el.Value.Item2 == y) == 0;
                Debug.Log($"Remove line - {y} = {sortedInp.Count(el => el.Value.Item2 == y)}");
                if (removeLine)
                {
                    maxY--;
                    foreach (var element in sortedInp.Where(el => el.Value.Item2 > y))
                    {
                        sorted2[element.Key] = new Tuple<int, int>(element.Value.Item1, element.Value.Item2 - 1);
                    }
                    y--;
                    sortedInp = new Dictionary<int, Tuple<int, int>>(sorted2);
                }
            }
        return sortedInp;
    }
}
