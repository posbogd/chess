﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Stage 
{
   SelectFigure,
   PutFigure
}
