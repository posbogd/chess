﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomMenu : MonoBehaviour
{
    [SerializeField]
    private Text _roomName;
    [SerializeField]
    private Text _playerName;


    public void SetRoomMenu(string roomName, string playerName)
    {
        _roomName.text = roomName;
        _playerName.text = playerName;
    }
}
